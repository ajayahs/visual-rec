import bpy , bmesh

path_of_faces = '/home/abhi/Desktop/visualRec/faces/'



faces_list = []
with open('/home/abhi/Desktop/visualRec/faces/all_faces.txt') as ch_file:
    for ch in ch_file:
            ch = ch.strip('\n')
                    faces_list.append(str(ch))

for f in faces_list:

    coords = []
    face_path = path_of_faces+'/'+f
    imported_face = bpy.ops.import_scene.obj(filepath=face_path)


    bpy.ops.object.select_all(action='DESELECT')

    ob = bpy.context.scene.objects 
    
    if not len( bpy.context.object.data.uv_layers ):
        bpy.ops.uv.smart_project()


    # Loops per face
    for face in ob.data.polygons:
        for vert_idx, loop_idx in zip(face.vertices, face.loop_indices):
            uv_coords = ob.data.uv_layers.active.data[loop_idx].uv
            coords.append([uv_coords.x , uv_coords.y])

    faces = np.asarray(coords)

    np.save('f' , faces)

