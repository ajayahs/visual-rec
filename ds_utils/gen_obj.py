from scipy.io import loadmat
import pymesh
import os
import os.path as osp
import numpy as np

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--path', default="/home/ajaya/Datasets/300W-3D-Face")
parser.add_argument('--save-dir', default="/home/ajaya/Datasets/300W-3D-Face-Obj")
parser.add_argument('--bfm', default="../meta/bfm_faces.npy")

def main():
    args = parser.parse_args()
    mat_root = args.path
    obj_root = args.save_dir

    if not osp.exists(obj_root):
        os.makedirs(obj_root)
    dbs = os.listdir(mat_root)

    bfm = np.load(args.bfm)
    bfm = bfm.transpose()
    bfm -= 1

    for db in dbs:
        print "Entering {}".format(db)
        mat_files_path = osp.join(mat_root, db)
        mat_files = os.listdir(mat_files_path)

        obj_files_path = osp.join(obj_root, db)
        if not osp.exists(obj_files_path):
            os.makedirs(obj_files_path)

        for i, file in enumerate(mat_files):

            full_file = osp.join(mat_files_path, file)
            face_vertices = loadmat(full_file)['Fitted_Face']
            face_vertices = face_vertices.transpose()
            obj_file = osp.join(obj_files_path, file.replace(".mat", ".obj"))

            mesh = pymesh.form_mesh(face_vertices, bfm)
            pymesh.save_mesh(obj_file,mesh, ascii=True, use_float=True)

            print "{0} : Wrote {1}".format(i, obj_file)

if __name__ =="__main__":
    main()
